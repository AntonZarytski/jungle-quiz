package com.Quiz.Jungle.animals.app.loader;

import android.content.res.Resources;

import com.Quiz.Jungle.animals.app.R;
import com.Quiz.Jungle.animals.app.entity.QuestionEntity;

import java.util.ArrayList;
import java.util.List;

public class QuestionLoaderUseCase {

    public static List<QuestionEntity> loadQuestions(Resources res) {
        String[] questionsArray = res.getStringArray(R.array.quiz_questions);
        String[] correctAnswersArray = res.getStringArray(R.array.correct_answers);
        String[] variants1Array = res.getStringArray(R.array.options_1);
        String[] variants2Array = res.getStringArray(R.array.options_2);
        String[] variants3Array = res.getStringArray(R.array.options_3);

        List<QuestionEntity> entities = new ArrayList<>();

        for (int i = 0; i < questionsArray.length; i++) {
            QuestionEntity question = new QuestionEntity(
                    questionsArray[i],
                    correctAnswersArray[i],
                    variants1Array[i],
                    variants2Array[i],
                    variants3Array[i],
                    correctAnswersArray[i]);
            entities.add(question);
        }

        return entities;
    }
}