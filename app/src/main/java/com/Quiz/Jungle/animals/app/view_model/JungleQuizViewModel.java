package com.Quiz.Jungle.animals.app.view_model;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.Quiz.Jungle.animals.app.entity.QuestionEntity;
import com.Quiz.Jungle.animals.app.models.QuizUseCase;

import java.util.Objects;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import java.util.Objects;

public class JungleQuizViewModel extends ViewModel {

    private final QuizUseCase model = QuizUseCase.getInstance();
    private final MutableLiveData<QuestionEntity> currentQuestion = new MutableLiveData<>();
    private final MutableLiveData<String> correctAnswer = new MutableLiveData<>();
    private final MutableLiveData<String> wrongAnswer = new MutableLiveData<>();
    private final MutableLiveData<String> score = new MutableLiveData<>();

    public void initQuiz() {
        model.reset();
        updateCurrentQuestion(model.getNextQuestion(null));
    }

    public void onResume(int questionIndex, String answer) {
        QuestionEntity current = fetchQuestionByIndex(questionIndex);
        setAnswerForQuestion(current, answer);
        updateCurrentQuestion(current);
        validateCurrentAnswer();
    }

    public LiveData<QuestionEntity> getCurrentQuestion() {
        return currentQuestion;
    }

    public int getCurrentQuestionPosition() {
        return model.indexOf(currentQuestion.getValue());
    }

    private void updateWrongAnswer(String userAnswer) {
        if (!Objects.requireNonNull(currentQuestion.getValue()).isCorrectAnswer()) {
            wrongAnswer.setValue(userAnswer);
        }
    }

    private void validateCurrentAnswer() {
        onAnswerWasSet(Objects.requireNonNull(currentQuestion.getValue()).getUserAnswer());
    }

    public LiveData<String> getCorrectAnswer() {
        return correctAnswer;
    }

    public LiveData<String> getScore() {
        return score;
    }

    public LiveData<String> getWrongAnswer() {
        return wrongAnswer;
    }

    private QuestionEntity fetchQuestionByIndex(int index) {
        return model.getQuestion(index);
    }

    private void setAnswerForQuestion(QuestionEntity question, String answer) {
        model.setAnswer(question, answer);
    }

    public void toTheNextQuestion() {
        QuestionEntity current = currentQuestion.getValue();
        if (current != null && current.isAnswered()) {
            proceedToNextQuestion();
        }
    }

    public void onAnswerWasSet(String userAnswer) {
        QuestionEntity current = currentQuestion.getValue();
        if (current != null && !current.isAnswered()) {
            setAnswerForQuestion(current, userAnswer);
            updateCorrectAnswer();
            updateWrongAnswer(userAnswer);
        }
    }

    private void updateCurrentQuestion(QuestionEntity question) {
        currentQuestion.setValue(question);
    }

    private void updateCorrectAnswer() {
        correctAnswer.setValue(Objects.requireNonNull(currentQuestion.getValue()).getCorrectAnswer());
    }

    private void proceedToNextQuestion() {
        QuestionEntity next = model.getNextQuestion(currentQuestion.getValue());
        if (next != null) {
            updateCurrentQuestion(next);
        } else {
            finalizeQuiz();
        }
    }

    private void finalizeQuiz() {
        score.setValue(model.countCorrectAnswersCount() + "/" + model.getQuestionCount());
    }
}