package com.Quiz.Jungle.animals.app;

import android.media.MediaPlayer;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.navigation.ui.NavigationUI;

import com.Quiz.Jungle.animals.app.databinding.ActivityMainBinding;
import com.Quiz.Jungle.animals.app.view_model.MainActivityViewModel;

import java.util.Objects;

public class MainActivity extends AppCompatActivity implements VolumeController, UIController {

    private MainActivityViewModel viewModel;
    private ActivityMainBinding binding;
    private MediaPlayer player;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        viewModel = new ViewModelProvider(this).get(MainActivityViewModel.class);
        viewModel.setSoundOn(true);

        configureAppNavigation(cofigureToolbar());
        startPlayer(R.raw.quiz_music);
    }

    @NonNull
    private Toolbar cofigureToolbar() {
        Toolbar toolbar = binding.toolbar;
        toolbar.setNavigationOnClickListener(view -> getOnBackPressedDispatcher().onBackPressed());
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        return toolbar;
    }

    private void configureAppNavigation(Toolbar toolbar) {
        NavHostFragment navHostFragment = (NavHostFragment) getSupportFragmentManager()
                .findFragmentById(R.id.nav_host_fragment);
        if (navHostFragment == null) {
            return;
        }
        NavController navController = navHostFragment.getNavController();
        NavigationUI.setupActionBarWithNavController(this, navController);
        toolbar.setNavigationOnClickListener(v -> {
            toMainMenuOrExit(navController);
        });
        navController.addOnDestinationChangedListener((controller, destination, arguments) -> {
            Objects.requireNonNull(getSupportActionBar())
                    .setDisplayHomeAsUpEnabled(destination.getId() != R.id.mainMenuFragment
                            && destination.getId() != R.id.exitFragment);
            blurBackgroundIfNeed(destination.getId());
        });
    }

    private void toMainMenuOrExit(NavController navController) {
        if (navController.getCurrentDestination() != null
                && navController.getCurrentDestination().getId() == R.id.mainMenuFragment) {
            finish();
        } else {
            navController.navigate(R.id.action_global_mainMenuFragment);
        }
    }

    private void blurBackgroundIfNeed(int currentDestinationId) {
        if (currentDestinationId == R.id.mainMenuFragment ||
                currentDestinationId == R.id.loadingFragment ||
                currentDestinationId == R.id.exitFragment
        ) {
            binding.backgroundImage.setImageResource(R.drawable.background);
        } else {
            binding.backgroundImage.setImageResource(R.drawable.background_blured);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return navController.navigateUp() || super.onSupportNavigateUp();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (viewModel.isSoundOn()) {
            resumePlayer();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        viewModel.setSoundOn(isSoundEnabled());
        pausePlayer();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopPlayer();
    }

    private void startPlayer(int phoneMusicId) {
        player = MediaPlayer.create(this, phoneMusicId);
        player.setLooping(true);
        player.start();
    }

    private void resumePlayer() {
        if (player != null && !player.isPlaying()) {
            player.start();
        }
    }

    private void pausePlayer() {
        if (player != null && player.isPlaying()) {
            player.pause();
        }
    }

    private void stopPlayer() {
        if (player != null) {
            if (player.isPlaying()) {
                player.stop();
            }
            player.release();
            player = null;
        }
    }

    @Override
    public void changeVolume(boolean isSoundOn) {
        if (isSoundOn) {
            pausePlayer();
        } else {
            resumePlayer();
        }
    }

    @Override
    public boolean isSoundEnabled() {
        return player != null && player.isPlaying();
    }

    @Override
    public void scrollDown() {
        binding.nestedScrollView.post(() ->
                binding.nestedScrollView.scrollTo(0, binding.nestedScrollView.getChildAt(0)
                        .getMeasuredHeight()));
    }
}