package com.Quiz.Jungle.animals.app;

interface VolumeController {
    void changeVolume(boolean isSoundEnabled);
    boolean isSoundEnabled();
}
