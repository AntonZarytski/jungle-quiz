package com.Quiz.Jungle.animals.app.view_model;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class MainActivityViewModel extends ViewModel {
    private final MutableLiveData<Boolean> isSoundOn = new MutableLiveData<>();

    public void setSoundOn(boolean soundOn) {
        isSoundOn.setValue(soundOn);
    }

    public Boolean isSoundOn() {
        return isSoundOn.getValue();
    }
}
