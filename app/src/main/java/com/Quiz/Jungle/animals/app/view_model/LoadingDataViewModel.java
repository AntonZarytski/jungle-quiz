package com.Quiz.Jungle.animals.app.view_model;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.Quiz.Jungle.animals.app.entity.QuestionEntity;
import com.Quiz.Jungle.animals.app.models.QuizUseCase;

import java.util.List;

public class LoadingDataViewModel extends ViewModel {


    private final QuizUseCase model = QuizUseCase.getInstance();
    public final MutableLiveData<Boolean> readyStatus = new MutableLiveData<>();

    public void setQuizQuestions(List<QuestionEntity> questionEntities) {
        model.setQuizQuestions(questionEntities);
        readyStatus.setValue(true);
    }
}