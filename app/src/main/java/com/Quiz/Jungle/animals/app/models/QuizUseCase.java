package com.Quiz.Jungle.animals.app.models;

import android.os.Build;

import com.Quiz.Jungle.animals.app.entity.QuestionEntity;

import java.util.ArrayList;
import java.util.List;

public class QuizUseCase {

    private static QuizUseCase instance;
    private final List<QuestionEntity> questionEntities;

    private QuizUseCase() {
        questionEntities = new ArrayList<>();
    }

    public static synchronized QuizUseCase getInstance() {
        if (instance == null) {
            instance = new QuizUseCase();
        }
        return instance;
    }

    public void setQuizQuestions(List<QuestionEntity> questionEntities) {
        this.questionEntities.clear();
        this.questionEntities.addAll(questionEntities);
    }

    public QuestionEntity getNextQuestion(QuestionEntity current) {
        if (current == null) {
            return getFirstQuestion();
        }
        return findNextQuestion(current);
    }

    public String getQuestionCount() {
        return String.valueOf(questionEntities.size());
    }

    public int indexOf(QuestionEntity value) {
        return questionEntities.indexOf(value);
    }

    public QuestionEntity getQuestion(int questionIndex) {
        return questionEntities.get(questionIndex);
    }

    public void reset() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            questionEntities.forEach(question -> question.setUserAnswer(null));
        } else {
            for (QuestionEntity question : questionEntities) {
                question.setUserAnswer(null);
            }
        }
    }

    public void setAnswer(QuestionEntity current, String answer) {
        QuestionEntity local = findQuestionEntity(current);
        if (local != null) {
            local.setUserAnswer(answer);
        }
    }

    public int countCorrectAnswersCount() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return (int) questionEntities.stream()
                    .filter(QuestionEntity::isCorrectAnswer)
                    .count();
        } else {
            int correctCount = 0;
            for (QuestionEntity question : questionEntities) {
                if (question.isCorrectAnswer()) {
                    correctCount++;
                }
            }
            return correctCount;
        }
    }

    private QuestionEntity getFirstQuestion() {
        return !questionEntities.isEmpty() ? questionEntities.get(0) : null;
    }

    private QuestionEntity findNextQuestion(QuestionEntity current) {
        int newIndex = questionEntities.indexOf(current) + 1;
        return newIndex < questionEntities.size() ? questionEntities.get(newIndex) : null;
    }

    private QuestionEntity findQuestionEntity(QuestionEntity current) {
        int index = questionEntities.indexOf(current);
        return index != -1 ? questionEntities.get(index) : null;
    }
}
