package com.Quiz.Jungle.animals.app;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.Quiz.Jungle.animals.app.databinding.QuizFragmentBinding;
import com.Quiz.Jungle.animals.app.view_model.JungleQuizViewModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class JungleQuizFragment extends Fragment {

    private JungleQuizViewModel viewModel;
    private UIController uiController;
    private QuizFragmentBinding binding;
    private String questionIndexKey;
    private String answerKey;
    private String resultKey;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        uiController = (UIController) context;
        questionIndexKey = getString(R.string.question_number);
        answerKey = getString(R.string.answer);
        resultKey = getString(R.string.quiz_result);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = QuizFragmentBinding.inflate(inflater, container, false);
        viewModel = new ViewModelProvider(this).get(JungleQuizViewModel.class);
        viewModel.initQuiz();

        viewModel.getCurrentQuestion().observe(getViewLifecycleOwner(), question -> {
            binding.questionTextView.setText(question.getQuestion());
            List<Button> buttons = getButtons();
            List<String> options = question.getOptions();
            for (int i = 0; i < buttons.size(); i++) {
                buttons.get(i).setText(options.get(i));
            }
            resetButtonsState();
            if (uiController != null) {
                uiController.scrollDown();
            }
        });

        viewModel.getCorrectAnswer().observe(getViewLifecycleOwner(), correctAnswer -> {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        getButtons().forEach(button -> {
                            if (button.getText().toString().equals(correctAnswer)) {
                                setButtonColor(button, R.drawable.correct_answer);
                            }
                        });
                    } else {
                        for (Button button : getButtons()) {
                            if (button.getText().toString().equals(correctAnswer)) {
                                setButtonColor(button, R.drawable.correct_answer);
                            }
                        }
                    }
                }
        );

        viewModel.getWrongAnswer().observe(getViewLifecycleOwner(), wrongAnswer -> {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        getButtons().forEach(button -> {
                            if (button.getText().toString().equals(wrongAnswer)) {
                                setButtonColor(button, R.drawable.wrong_answer);
                            }
                        });
                    } else {
                        for (Button button : getButtons()) {
                            if (button.getText().toString().equals(wrongAnswer)) {
                                setButtonColor(button, R.drawable.wrong_answer);
                            }
                        }
                    }
                }
        );

        viewModel.getScore().observe(getViewLifecycleOwner(), score -> {
            Bundle bundle = new Bundle();
            bundle.putString(resultKey, score);
            NavController navController = Navigation.findNavController(requireActivity(), R.id.nav_host_fragment);
            navController.navigate(R.id.action_quizFragment_to_scoreFragment, bundle);
        });

        binding.nextQuestionButton.setOnClickListener(v -> viewModel.toTheNextQuestion());

        if (savedInstanceState != null) {
            String answer = savedInstanceState.getString(answerKey, null);
            int questionIndex = savedInstanceState.getInt(questionIndexKey, 0);
            viewModel.onResume(questionIndex, answer);
        }

        initListeners();
        return binding.getRoot();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        restoreQuizData(outState);
    }

    private void restoreQuizData(@NonNull Bundle outState) {
        int index = viewModel.getCurrentQuestionPosition();
        String answer = Objects.requireNonNull(viewModel.getCurrentQuestion().getValue())
                .getUserAnswer();
        outState.putInt(questionIndexKey, index);
        outState.putString(answerKey, answer);
    }

    @NonNull
    private List<Button> getButtons() {
        return new ArrayList<>(Arrays.asList(binding.answerVariant1,
                                             binding.answerVariant2,
                                             binding.answerVariant3,
                                             binding.answerVariant4));
    }

    private void initListeners() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            getButtons().forEach(this::setButtonListener);
        } else {
            for (Button button : getButtons()) {
                setButtonListener(button);
            }
        }
    }

    private void setButtonListener(@NonNull Button button) {
        button.setOnClickListener(v -> viewModel.onAnswerWasSet(button.getText().toString()));
    }

    private void resetButtonsState() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            getButtons().forEach(button -> setButtonColor(button, R.drawable.question_btn_selector));
        } else {
            for (Button button : getButtons()) {
                setButtonColor(button, R.drawable.question_btn_selector);
            }
        }
    }

    private void setButtonColor(@NonNull Button button, int drawableRes) {
        button.setBackground(ContextCompat.getDrawable(requireContext(), drawableRes));
    }
}

