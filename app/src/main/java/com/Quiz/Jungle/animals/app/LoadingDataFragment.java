package com.Quiz.Jungle.animals.app;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.Quiz.Jungle.animals.app.databinding.LoadingFragmentBinding;
import com.Quiz.Jungle.animals.app.loader.QuestionLoaderUseCase;
import com.Quiz.Jungle.animals.app.view_model.LoadingDataViewModel;

public class LoadingDataFragment extends Fragment {

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        LoadingFragmentBinding binding = LoadingFragmentBinding.inflate(inflater, container, false);
        configureViewModel();
        return binding.getRoot();
    }

    private void configureViewModel() {
        LoadingDataViewModel vm = new ViewModelProvider(this).get(LoadingDataViewModel.class);
        vm.setQuizQuestions(QuestionLoaderUseCase.loadQuestions(requireContext().getResources()));
        vm.readyStatus.observe(requireActivity(), isReady -> {
            if (isReady) {
                toMainMenu();
            }
        });
    }

    private void toMainMenu() {
        NavController navController = Navigation.findNavController(requireActivity(), R.id.nav_host_fragment);
        navController.navigate(R.id.action_loadingFragment_to_mainMenuFragment);
    }
}
