package com.Quiz.Jungle.animals.app;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.Quiz.Jungle.animals.app.databinding.MainMenuFragmentBinding;

public class MainMenuFragment extends Fragment {

    private MainMenuFragmentBinding binding;
    private VolumeController volumeController;
    private String soundOff;
    private String soundOn;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        volumeController = (VolumeController) context;
        soundOff = getString(R.string.sound_off);
        soundOn = getString(R.string.sound_on);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = MainMenuFragmentBinding.inflate(inflater, container, false);
        initListeners(binding);
        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        configureButtons();
    }

    private void configureButtons() {
        if (volumeController != null) {
            if (volumeController.isSoundEnabled()) {
                binding.changeSound.setText(soundOn);
            } else {
                binding.changeSound.setText(soundOff);
            }
        }
    }

    private void initListeners(@NonNull MainMenuFragmentBinding binding) {
        NavController navigation = Navigation.findNavController(requireActivity(), R.id.nav_host_fragment);
        binding.beginQuiz.setOnClickListener(v -> {
            navigation.navigate(R.id.action_mainMenuFragment_to_quizFragment);
        });
        binding.changeSound.setOnClickListener(v -> {
            if (volumeController != null) {
                boolean isSoundOn = binding.changeSound.getText().toString().equals(soundOn);
                volumeController.changeVolume(isSoundOn);
                if (isSoundOn) {
                    binding.changeSound.setText(soundOff);
                } else {
                    binding.changeSound.setText(soundOn);
                }

            }

        });
        binding.appExit.setOnClickListener(v -> navigation.navigate(R.id.action_mainMenuFragment_to_exitFragment));
    }
}
