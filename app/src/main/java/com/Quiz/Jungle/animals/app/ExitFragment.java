package com.Quiz.Jungle.animals.app;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.Quiz.Jungle.animals.app.databinding.ExitFragmentBinding;

public class ExitFragment extends Fragment {

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ExitFragmentBinding binding = ExitFragmentBinding.inflate(inflater, container, false);
        initListeners(binding);
        return binding.getRoot();
    }

    private void initListeners(ExitFragmentBinding binding) {
        binding.yesBtn.setOnClickListener(v -> {
            requireActivity().finish();
        });
        binding.noBtn.setOnClickListener(v -> {
            NavController navController = Navigation.findNavController(requireActivity(), R.id.nav_host_fragment);
            navController.navigate(R.id.action_exitFragment_to_mainMenuFragment);
        });
    }
}
