package com.Quiz.Jungle.animals.app;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.Quiz.Jungle.animals.app.databinding.ScoreFragmentBinding;

public class ScoreFragment extends Fragment {

    private String resultKey;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        resultKey = getString(R.string.quiz_result);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ScoreFragmentBinding binding = ScoreFragmentBinding.inflate(inflater, container, false);
        restore(binding);
        initListeners(binding);
        return binding.getRoot();
    }

    private void restore(ScoreFragmentBinding binding) {
        if (getArguments() != null) {
            String score = getArguments().getString(resultKey);
            binding.scoreTextView.setText(score);
        }
    }

    private void initListeners(ScoreFragmentBinding binding) {
        binding.toMainMenu.setOnClickListener(view -> {
            NavController navigation = Navigation.findNavController(requireActivity(), R.id.nav_host_fragment);
            navigation.navigate(R.id.action_quizResultFragment_to_mainMenuFragment);
        });
    }
}