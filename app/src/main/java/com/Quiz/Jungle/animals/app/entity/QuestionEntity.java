package com.Quiz.Jungle.animals.app.entity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class QuestionEntity {
    private final String question;
    private final List<String> options;
    private final String correctAnswer;
    private String userAnswer;

    public QuestionEntity(@NonNull String question,
                          @NonNull String option1,
                          @NonNull String option2,
                          @NonNull String option3,
                          @NonNull String option4,
                          @NonNull String correctAnswer) {
        this.question = question;
        this.options = Arrays.asList(option1, option2, option3, option4);
        Collections.shuffle(this.options);
        this.correctAnswer = correctAnswer;
    }

    @NonNull
    public List<String> getOptions() {
        return options;
    }

    @NonNull
    public String getQuestion() {
        return question;
    }

    public boolean isCorrectAnswer() {
        return correctAnswer.equals(userAnswer);
    }

    @NonNull
    public String getCorrectAnswer() {
        return correctAnswer;
    }

    @Nullable
    public String getUserAnswer() {
        return userAnswer;
    }

    public void setUserAnswer(@Nullable String answer) {
        userAnswer = answer;
    }

    public boolean isAnswered() {
        return userAnswer != null;
    }
}
